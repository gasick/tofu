provider "local" {}

resource "local_file" "example" {
  content  = "Hello, this is a sample file created by Terraform!"
  filename = "${path.module}/sample_file.txt"
}